#include<stdio.h>
int main(){
	printf(".:::Program to find the volume of the cone:::.\n");
	int height,rad,volume=0; //Declaring varialbles
	printf("Enter the radius of cone: ");
	scanf("%i",&rad); //get a value for radius
	printf("Enter the height of cone: ");
	scanf("%i",&height); //get a value for height
	volume=3.14159*rad*rad*height/3; //equation for volume of a cone
	printf("Volume of the cone is : ");
	printf("%i",volume); //displaying volume of the cone
	
	return 0;
}
