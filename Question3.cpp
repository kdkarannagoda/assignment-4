#include<stdio.h>
int main(){

	int num1,num2=0; //Declaring variables
	printf("Enter the number one : ");
	scanf("%i",&num1); //asking user to enter a value for number one
	printf("Enter the number two : ");
	scanf("%i",&num2); //asking user to enter a value for number two
	printf("Before swapping number one: %i, number two : %i \n",num1,num2);	//display before swapping
	//swapping numbers
	num1=num1+num2;
	num2=num1-num2;
	num1=num1-num2;
	printf("After swapping number one: %i, number two : %i ",num1,num2); //display after swapping
	
	return 0;
}

