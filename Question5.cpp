#include<stdio.h>
int main(){
	int A=10,B=15; //declaring variables
	printf("A = 10 and B =15\n"); 
	printf("result of\tA & B\tis\t: %i\n",A&B);//AND operator
	printf("result of\tA ^ B\tis\t: %i\n",A^B);//OR operator
	printf("result of\t~A\tis\t: %i\n",~A);//NOT operator
	printf("result of\tA<<3\tis\t: %i\n",A<<3);//LEFT SHIFT
	printf("result of\tB>>3\tis\t: %i\n",B>>3);//RIGHT SHIFT
	
	return 0;
}
